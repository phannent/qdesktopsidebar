#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "systeminfo.h"
#ifdef Q_OS_WIN
#include "systeminfo_windows.h"
#elif defined(Q_OS_LINUX)
#include "systeminfo_linux.h"
#else
#include "systeminfo_mac.h"
#endif

#include <QDebug>
#include <QDesktopWidget>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_sidebar_width(250),
    m_taskbar_height(40),
    m_system_info_updater(0),
    m_system_info(0)
{
    setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    ui->setupUi(this);

    setGeometry(QApplication::desktop()->screenGeometry().width() - m_sidebar_width,
                0,
                m_sidebar_width,
                QApplication::desktop()->screenGeometry().height() - m_taskbar_height);

    m_system_info_updater = new QTimer(this);
    m_system_info_updater->setInterval(2500); /* Update every 2.5 sec */
    m_system_info_updater->setSingleShot(false);
#ifdef Q_OS_WIN
    m_system_info = new system_info_windows(this);
#elif defined(Q_OS_LINUX)
    m_system_info = new system_info_linux(this);
#else
    m_system_info = new system_info_mac(this);
#endif
    connect(m_system_info, SIGNAL(signal_system_data(system_data*)),
            this, SLOT(slot_update_system_info(system_data*)));
    connect(m_system_info_updater, SIGNAL(timeout()),
            m_system_info, SLOT(slot_get_system_data()));

    m_system_info_updater->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_toolButton_clicked()
{
    qApp->quit();
}

void MainWindow::slot_update_system_info(system_data* d)
{
   qDebug() << "MainWindow::slot_update_system_info" << d->cpu_pc;
}

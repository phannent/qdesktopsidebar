#include "systeminfo.h"

#include <QDebug>

system_info::system_info(QObject *parent) :
    QObject(parent)
{

}

void system_info::slot_get_system_data()
{
    system_data* d = new system_data;
    d->cpu_pc = get_cpu_pc();
    d->disk_io_bytes = get_disk_io_bytes();
    d->disk_io_ops = get_disk_io_ops();
    d->memory_pc = get_memory_pc();
    d->net_bytes = get_net_bytes();
    emit signal_system_data(d);
}

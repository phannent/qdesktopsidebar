#-------------------------------------------------
#
# Project created by QtCreator 2014-01-28T11:36:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qsidebar
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    systeminfo.cpp \
    systeminfo_windows.cpp \
    systeminfo_linux.cpp \
    systeminfo_mac.cpp

HEADERS  += mainwindow.h \
    systeminfo.h \
    systeminfo_windows.h \
    systeminfo_linux.h \
    systeminfo_mac.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    README.md

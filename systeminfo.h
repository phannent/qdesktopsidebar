#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

#include <QObject>

struct system_data {
    qreal cpu_pc;
    quint64 net_bytes;
    quint64 disk_io_bytes;
    quint64 disk_io_ops;
    qreal memory_pc;
};

class system_info : public QObject
{
    Q_OBJECT
public:
    explicit system_info(QObject *parent = 0);

signals:
    void signal_system_data(system_data* d);

public slots:
    void slot_get_system_data();

private:
    virtual qreal get_cpu_pc() = 0;
    virtual quint64 get_net_bytes() = 0;
    virtual quint64 get_disk_io_bytes() = 0;
    virtual quint64 get_disk_io_ops() = 0;
    virtual qreal get_memory_pc() = 0;

};

#endif // SYSTEMINFO_H

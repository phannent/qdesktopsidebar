#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "systeminfo.h"

namespace Ui {
class MainWindow;
}

class QTimer;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void signal_get_system_info();

private slots:
    void on_toolButton_clicked();
    void slot_update_system_info(system_data* d);

private:
    Ui::MainWindow *ui;
    int m_sidebar_width;
    int m_taskbar_height;
    QTimer* m_system_info_updater;
    system_info* m_system_info;
};

#endif // MAINWINDOW_H

#ifndef SYSTEMINFO_LINUX_H
#define SYSTEMINFO_LINUX_H

#include "systeminfo.h"

#include <QObject>

class system_info_linux : public system_info
{
    Q_OBJECT
public:
    explicit system_info_linux(QObject *parent = 0);

signals:

public slots:

private:
    qreal get_cpu_pc();
    quint64 get_net_bytes();
    quint64 get_disk_io_bytes();
    quint64 get_disk_io_ops();
    qreal get_memory_pc();

};

#endif // SYSTEMINFO_LINUX_H

#ifndef SYSTEMINFO_WINDOWS_H
#define SYSTEMINFO_WINDOWS_H

#include "systeminfo.h"

#include <QObject>

class system_info_windows : public system_info
{
    Q_OBJECT
public:
    explicit system_info_windows(QObject *parent = 0);

private:
    qreal get_cpu_pc();
    quint64 get_net_bytes();
    quint64 get_disk_io_bytes();
    quint64 get_disk_io_ops();
    qreal get_memory_pc();
};

#endif // SYSTEMINFO_WINDOWS_H

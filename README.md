This is my new Sidebar project, I was disheartened to see that Google had
cancelled their sidebar which provided me with system information and an
up to date calendar.

The plan here is:
1. Create a widget that is frame-less and always on top
2. The widget has the height of the current desktop, less the task bar height
3. The widgets width is <10% of screen space
4. The widget contains three areas
5. The first area contains charts
6. The second area contains my Google calendar
7. The third area to contain the currently selected days events
8. Going to need an API for accessing CPU info in a cross platform way
9. Going to need an API to access Google services (calendar)

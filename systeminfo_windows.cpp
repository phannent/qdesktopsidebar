#include "systeminfo_windows.h"

#include <QDebug>

system_info_windows::system_info_windows(QObject *parent) :
    system_info(parent)
{
}

qreal system_info_windows::get_cpu_pc()
{
    qDebug() << "system_info_windows::get_cpu_pc()";
    qreal r = 0.0;

    return r;
}

quint64 system_info_windows::get_net_bytes()
{
    qDebug() << "system_info_windows::get_net_bytes()";
    quint64 b = 0;

    return b;
}

quint64 system_info_windows::get_disk_io_bytes()
{
    qDebug() << "system_info_windows::get_disk_io_bytes()";
    quint64 b = 0;

    return b;
}

quint64 system_info_windows::get_disk_io_ops()
{
    qDebug() << "system_info_windows::get_disk_io_ops()";
    quint64 b = 0;

    return b;
}

qreal system_info_windows::get_memory_pc()
{
    qDebug() << "system_info_windows::get_memory_pc()";
    qreal r = 0.0;

    return r;
}

#include "systeminfo_mac.h"

#include <QDebug>

system_info_mac::system_info_mac(QObject *parent) :
    system_info(parent)
{
}

qreal system_info_mac::get_cpu_pc()
{
    qDebug() << "system_info_mac::get_cpu_pc()";
    qreal r = 0.0;

    return r;
}

quint64 system_info_mac::get_net_bytes()
{
    qDebug() << "system_info_mac::get_net_bytes()";
    quint64 b = 0;

    return b;
}

quint64 system_info_mac::get_disk_io_bytes()
{
    qDebug() << "system_info_mac::get_disk_io_bytes()";
    quint64 b = 0;

    return b;
}

quint64 system_info_mac::get_disk_io_ops()
{
    qDebug() << "system_info_mac::get_disk_io_ops()";
    quint64 b = 0;

    return b;
}

qreal system_info_mac::get_memory_pc()
{
    qDebug() << "system_info_mac::get_memory_pc()";
    qreal r = 0.0;

    return r;
}
